package registerstudents;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class TestOperation
 {
     private static final Logger logger = LogManager.getLogger(TestOperation.class);

     public static void main(String args[])
     {
         StudentDetail studentDetail = new StudentDetail();
         studentDetail.setName("Atharva");
         studentDetail.setField("Science");
         studentDetail.setMarks(450);
         Transaction transaction=null;

         try(Session session = HibernateConfig.getSessionFactory().openSession())
         {
             transaction = session.beginTransaction();
             ValidatorFactory validatorFactory= Validation.buildDefaultValidatorFactory();
             Validator validator=validatorFactory.getValidator();
             Set<ConstraintViolation<StudentDetail>> setError=validator.validate(studentDetail);
             if(!setError.isEmpty())
             {
                 for(ConstraintViolation<StudentDetail> studentDetailConstraintViolation:setError)
                 {
                     logger.error("{} {}",studentDetailConstraintViolation.getPropertyPath()
                             ,studentDetailConstraintViolation.getMessage());

                 }
             }
             else
                 {
                     session.saveOrUpdate(studentDetail);
                     transaction.commit();
                 }

         } catch (Exception e)
         {
             logger.error(e);
             if(transaction!=null)
             {
                 transaction.rollback();
             }

         } finally
         {
             HibernateConfig.shutdown();
         }
     }
 }
