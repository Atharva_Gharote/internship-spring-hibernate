package registerstudents;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import util.HibernateUtil;

public class CacheOperation
 {
     private static final Logger logger = LogManager.getLogger(CacheOperation.class);

     public static void main(String args[])
     {
         try( Session session = HibernateUtil.getSession();
              Session session1= HibernateUtil.getSession())
         {
              logger.debug("Using first session object");
              StudentDetail studentDetail =session.get(StudentDetail.class,2);
              logger.info("{}",studentDetail.getId());
              logger.info("{}",studentDetail.getName());
              logger.info("{}",studentDetail.getField());
              logger.info("{}",studentDetail.getMarks());

              StudentDetail studentDetail2=session.get(StudentDetail.class,2);
              logger.info("{}",studentDetail2.getId());
              logger.info("{}",studentDetail2.getName());
              logger.info("{}",studentDetail2.getField());
              logger.info("{}",studentDetail2.getMarks());

             logger.debug("Using second session object");
             StudentDetail studentDetail1 =session1.get(StudentDetail.class,2);
             logger.info("{}",studentDetail1.getId());
             logger.info("{}",studentDetail1.getName());
             logger.info("{}",studentDetail1.getField());
             logger.info("{}",studentDetail1.getMarks());

         }catch (Exception e)
         {
            logger.error(e);
         }
         finally
         {
            HibernateUtil.closeSessionFactory();
         }
     }
 }
