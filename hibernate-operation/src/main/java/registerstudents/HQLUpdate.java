package registerstudents;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class HQLUpdate
 {
     private static final Logger logger = LogManager.getLogger(HQLUpdate.class);

     public static void main(String args[])
     {
         Transaction transaction=null;
         try(Session session = HibernateConfig.getSessionFactory().openSession())
         {
              transaction = session.beginTransaction();
              logger.debug("Updation Operation");
              Query query = session.createQuery("update StudentDetail set field='Arts' where id=3");
              int number = query.executeUpdate();
              logger.info("Rows Affected {}", number);
              logger.info("Selection of Rows");

              logger.debug("For first time query");
              Query query1=session.getNamedQuery("Student");
              query1.setParameter("id","2");
              query1.setCacheable(true);

             StudentDetail studentDetails=(StudentDetail)query1.uniqueResult();
             logger.info("Id={}", studentDetails.getId());
             logger.info("Name={}", studentDetails.getName());
             logger.info("Field={}", studentDetails.getField());
             logger.info("Marks={}", studentDetails.getMarks());

             logger.debug("For  second time query");
             Query query2=session.getNamedQuery("Student");
             query2.setParameter("id","2");
             query2.setCacheable(true);

             StudentDetail studentDetail=(StudentDetail)query2.uniqueResult();
             logger.info("Id={}", studentDetail.getId());
             logger.info("Name={}", studentDetail.getName());
             logger.info("Field={}", studentDetail.getField());
             logger.info("Marks={}", studentDetail.getMarks());
             transaction.commit();

         }catch (Exception e)
         {
             logger.error(e);
             if(transaction!=null)
             {
                 transaction.rollback();
             }

         }finally
         {
             HibernateConfig.shutdown();
         }
     }
 }
