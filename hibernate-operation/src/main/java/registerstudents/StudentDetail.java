package registerstudents;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;


@NamedQueries({@NamedQuery(name = "Student",query="from StudentDetail where id=:id")})
@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "student_details")

public class StudentDetail
{
    @Id()
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    @NotNull(message = "Name should not be empty")
    private String name;
    @Column(name = "field")
    @NotNull(message = "Field should not be empty")
    private String field;
    @Column(name = "marks")
    @NotNull
    @Max(value = 600,message = "Marks should not greater than 600")
    private int marks;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMarks(int marks)
    {
        this.marks = marks;
    }

    public int getMarks()
    {
        return marks;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }
}

