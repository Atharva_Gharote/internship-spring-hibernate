package employeedetails;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class Operation
 {
     private static final Logger logger = LogManager.getLogger(Operation.class);
     public static void main(String args[])
     {
         Manager manager=new Manager();
         manager.setId(222);
         manager.setName("Atharva");
         manager.setPassword("123456");
         manager.setBranch("Nagpur");

         Admin admin=new Admin();
         admin.setId(333);
         admin.setName("Prince");
         admin.setPassword("12");
         admin.setPost("President");
         Transaction transaction=null;

         try(Session session = HibernateUtil.getSession())
         {
             transaction = session.beginTransaction();
             session.save(manager);
             session.save(admin);
             transaction.commit();
         }
         catch (Exception e)
         {
             logger.error(e);
             if(transaction!=null)
             {
                 transaction.rollback();
             }
         }
         finally
         {
             HibernateUtil.closeSessionFactory();
         }
     }
 }
