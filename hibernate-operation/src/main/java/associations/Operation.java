package associations;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Operation
 {
    private static final Logger logger = LogManager.getLogger(Operation.class);

    public static void main(String args[])
    {
        Car car=new Car();
        car.setCompanyName("Renault");
        car.setModelName("Duster");

        Person person=new Person();
        person.setName("Ram V");
        person.setCar(car);
        Transaction transaction=null;

        try(Session session = HibernateConfig.getSessionFactory().openSession())
        {
            transaction=session.beginTransaction();
            session.save(car);
            session.save(person);
            transaction.commit();

        }catch (Exception e)
        {
            logger.error(e);
            if(transaction!=null)
            {
                transaction.rollback();
            }
        }
        finally
        {
            HibernateConfig.shutdown();
        }
    }
 }
