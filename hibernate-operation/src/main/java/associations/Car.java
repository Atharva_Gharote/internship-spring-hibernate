package associations;
import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Car
 {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     @Column(name = "id")
     private int id;
     @Column(name = "company_name")
     private String companyName;
     @Column(name = "model_name")
     private String modelName;

     public int getId()
     {
         return id;
     }

     public void setId(int id)
     {
         this.id = id;
     }

     public void setCompanyName(String companyName)
     {
         this.companyName = companyName;
     }

     public String getCompanyName()
     {
         return companyName;
     }

     public void setModelName(String modelName)
     {
         this.modelName = modelName;
     }

     public String getModelName()
     {
         return modelName;
     }

 }
