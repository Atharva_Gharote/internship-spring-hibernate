package associations;
import javax.persistence.*;

@Entity
@Table(name = "companies")
public class Company
 {
     @Id
     @GeneratedValue
     @Column(name = "id")
     private int id;
     @Column(name = "name")
     private String name;

     public int getId()
     {
         return id;
     }

     public void setId(int id)
     {
         this.id = id;
     }

     public void setName(String name)
     {
         this.name = name;
     }

     public String getName()
     {
         return name;
     }

 }
