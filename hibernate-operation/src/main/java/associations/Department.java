package associations;
import javax.persistence.*;
@Entity
@Table(name = "departments")
public class Department
 {
     @Id
     @GeneratedValue
     @Column(name = "id")
     private int id;
     @Column(name = "name")
     private String name;

     @ManyToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "company_id",referencedColumnName = "id")
     private Company company;

     public void setId(int id)
     {
         this.id = id;
     }

     public int getId()
     {
         return id;
     }

     public void setName(String name)
     {
         this.name = name;
     }

     public String getName()
     {
         return name;
     }

     public void setCompany(Company company)
     {
         this.company = company;
     }

     public Company getCompany()
     {
         return company;
     }
 }
