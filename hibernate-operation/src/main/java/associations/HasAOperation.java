package associations;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import util.HibernateUtil;

public class HasAOperation

 {
    private static final Logger logger = LogManager.getLogger(HasAOperation.class);

    public static void main(String args[])
    {
        Company company = new Company();
        company.setName("TCS");

        Department department = new Department();
        department.setName("Server");
        department.setCompany(company);

        Department department1 = new Department();
        department1.setName("Technical");
        department1.setCompany(company);

        Transaction transaction=null;

        try(Session session = HibernateUtil.getSession())
        {
            transaction = session.beginTransaction();
            session.save(department);
            session.save(department1);
            session.save(company);
            transaction.commit();

        } catch (Exception e)
        {
            logger.error(e);
            if(transaction!=null)
            {
             transaction.rollback();
            }
        }
        finally
        {
           HibernateUtil.closeSessionFactory();
        }
    }
 }