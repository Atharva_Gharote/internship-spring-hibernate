package associations;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import registerstudents.StudentDetail;
import loginapplication.Employee;

import java.util.HashMap;
import java.util.Map;

public class HibernateConfig
 {
     private static final Logger logger = LogManager.getLogger(HibernateConfig.class);
     private static StandardServiceRegistry registry;
     private static SessionFactory sessionFactory;

     public static SessionFactory getSessionFactory()
     {
         if (sessionFactory == null)
         {
             try
             {
                 StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
                 Map<String, String> settings = new HashMap<>();
                 settings.put(Environment.DRIVER, "org.postgresql.Driver");
                 settings.put(Environment.URL, "jdbc:postgresql://localhost:5432/postgres");
                 settings.put(Environment.USER, "postgres");
                 settings.put(Environment.PASS, "2317");
                 settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL94Dialect");
                 settings.put(Environment.CONNECTION_PROVIDER,"org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
                 settings.put(Environment.SHOW_SQL,"true");
                 settings.put(Environment.FORMAT_SQL,"true");

                 settings.put(Environment.USE_QUERY_CACHE,"true");
                 settings.put(Environment.USE_SECOND_LEVEL_CACHE,"true");
                 settings.put(Environment.CACHE_REGION_FACTORY,"org.hibernate.cache.ehcache.EhCacheRegionFactory");
                 settings.put(Environment.HBM2DDL_AUTO,"create");

                 settings.put("hibernate.hikari.connectionTimeout", "20000");
                 settings.put("hibernate.hikari.minimumIdle", "10");
                 settings.put("hibernate.hikari.maximumPoolSize", "50");
                 settings.put("hibernate.hikari.idleTimeout", "300000");

                 registryBuilder.applySettings(settings);
                 registry = registryBuilder.build();
                 MetadataSources metadataSources = new MetadataSources(registry);
                 metadataSources.addAnnotatedClass(Person.class);
                 metadataSources.addAnnotatedClass(Car.class);
                 metadataSources.addAnnotatedClass(Company.class);
                 metadataSources.addAnnotatedClass(Department.class);
                 metadataSources.addAnnotatedClass(StudentDetail.class);
                 metadataSources.addAnnotatedClass(Employee.class);
                 Metadata metadata = metadataSources.getMetadataBuilder().build();
                 sessionFactory = metadata.getSessionFactoryBuilder().build();

             } catch (Exception e)
             {
                 logger.error(e);
                 if (registry != null)
                 {
                     StandardServiceRegistryBuilder.destroy(registry);
                 }
             }
         }
         return sessionFactory;
     }
     public static void shutdown()
     {
         if (registry != null)
         {
             StandardServiceRegistryBuilder.destroy(registry);
         }
     }
 }
