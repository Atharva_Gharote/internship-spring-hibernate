package associations;
import javax.persistence.*;

@Entity
@Table(name = "owners")
public class Person
 {
     @Id
     @GeneratedValue
     @Column(name = "id")
     private int id;
     @Column(name = "name")
     private String name;

     @OneToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "car_id",referencedColumnName = "id")
     private Car car;


     public void setId(int id)
     {
         this.id = id;
     }

     public int getId()
     {
         return id;
     }

     public void setName(String name)
     {
         this.name = name;
     }

     public String getName()
     {
         return name;
     }

     public void setCar(Car car)
     {
         this.car = car;
     }

     public Car getCar()
     {
         return car;
     }
 }
