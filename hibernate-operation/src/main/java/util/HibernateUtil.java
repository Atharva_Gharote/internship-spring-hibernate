package util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil
 {
   private static SessionFactory sessionFactory;
   private static final Logger logger = LogManager.getLogger(HibernateUtil.class);

   static
   {
       try
       {
           Configuration configuration=new Configuration();
           configuration.configure("configuration.cfg.xml");
           sessionFactory=configuration.buildSessionFactory();

       }catch (Exception e)
       {
           logger.error(e);
       }
   }

   public static Session getSession()
   {

       return sessionFactory.openSession();
   }

    public static void closeSessionFactory()
    {
        sessionFactory.close();

    }
 }
