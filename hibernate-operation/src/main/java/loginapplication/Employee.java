package loginapplication;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "employee_details")
public class Employee
 {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     @Column(name = "id")
     private int id;
     @Column(name = "name")
     @NotNull
     private String name;
     @Column(name = "post")
     @NotNull
     private String post;
     @Column(name = "password")
     @NotNull(message = "Password Should not be empty")
     private String password;

     public Employee()
     {

     }

     public Employee(int id, String name, String post, String password)
     {
         super();
         this.id=id;
         this.name=name;
         this.post=post;
         this.password=password;
     }

     public int getId()
     {
         return id;
     }

     public void setId(int id)
     {
         this.id = id;
     }

     public String getName()
     {
         return name;
     }

     public void setName(String name)
     {
         this.name = name;
     }

     public void setPost(String post)
     {
         this.post = post;
     }

     public String getPost()
     {
         return post;
     }

     public void setPassword(String password)
     {
         this.password = password;
     }

     public String getPassword()
     {
         return password;
     }
 }

