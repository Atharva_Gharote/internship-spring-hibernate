package loginapplication;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class EmployeeServlet extends HttpServlet
 {

     public void doPost(HttpServletRequest request, HttpServletResponse response)
             throws IOException
     {
         response.setContentType("text/html");
         PrintWriter printWriter = response.getWriter();
         String name=(String)request.getAttribute("name");
         printWriter.println("<h1>Employee page</h1>"+
                             "<h3>Welcome "+name+"</h3>");

     }
 }
