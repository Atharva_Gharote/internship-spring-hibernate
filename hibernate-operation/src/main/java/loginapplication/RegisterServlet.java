package loginapplication;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.Transaction;

import java.io.IOException;
import java.util.Set;

public class RegisterServlet extends HttpServlet
 {
      private static final Logger logger = LogManager.getLogger(RegisterServlet.class);
      private int primaryKey;

     @Override
     protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)throws IOException
     {
         Transaction transaction=null;

         httpServletResponse.setContentType("text/html");
         String name = httpServletRequest.getParameter("name");
         String post=httpServletRequest.getParameter("post");
         String password=httpServletRequest.getParameter("password");
         Employee employee = new Employee(0, name, post,password);

         try(Session session = HibernateConfig.getSessionFactory().openSession())
         {
             transaction = session.beginTransaction();
             ValidatorFactory validatorFactory= Validation.buildDefaultValidatorFactory();
             Validator validator=validatorFactory.getValidator();
             Set<ConstraintViolation<Employee>> setError= validator.validate(employee);

             if(!setError.isEmpty())
             {
                 for(ConstraintViolation<Employee> employeeConstraintViolation:setError)
                 {
                     logger.error("{} {}",employeeConstraintViolation.getPropertyPath()
                             ,employeeConstraintViolation.getMessage());

                 }
             }
             else
             {
                 primaryKey= (Integer)session.save(employee);
                 transaction.commit();
             }
         }
         catch (Exception e)
         {
            logger.error(e);
            if(transaction!=null)
            {
                transaction.rollback();
            }
         }

         httpServletResponse.getWriter().println("Registration success Remember id="+primaryKey);
     }
     @Override
     public void destroy()
     {
        HibernateConfig.shutdown();
     }
 }


