package loginapplication;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;


public class CriteriaTest
 {
     private static final Logger logger = LogManager.getLogger(CriteriaTest.class);

     public static void main(String args[])
     {
         try(Session session = HibernateConfig.getSessionFactory().openSession())
         {
             logger.debug("Complete Employee Details");
             Criteria criteria = session.createCriteria(Employee.class);
             List<Employee> list = criteria.list();

             for (Employee employee : list)
             {
                 logger.info("Id {}", employee.getId());
                 logger.info("Name {}", employee.getName());
                 logger.info("post {}", employee.getPost());
                 logger.info("Password {}",employee.getPassword());

             }

             logger.debug("Single Record using restrictions");
             Criterion criterion = Restrictions.eq("id", 35);
             criteria.add(criterion);
             Employee employee = (Employee) criteria.uniqueResult();

             logger.info("Id = {}", employee.getId());
             logger.info("Name = {}", employee.getName());
             logger.info("post", employee.getPost());
             logger.info("Password {}",employee.getPassword());

             logger.debug("Single Column Entries using projections");
             Projection projection = Projections.property("name");
             criteria.setProjection(projection);

             List<String> list1 = criteria.list();
             for (String name : list1)
             {
                 logger.info("Name {}", name);

             }
         }
         catch (Exception e)
         {
             logger.error(e);
         }
         finally
         {
             HibernateConfig.shutdown();
         }

     }
 }
