package loginapplication;
import associations.HibernateConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet
 {
     private static final Logger logger = LogManager.getLogger(LoginServlet.class);

     @Override
     protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)throws IOException
     {
         Employee employee;
         RequestDispatcher requestDispatcher;
         httpServletResponse.setContentType("text/html");
         int id = Integer.parseInt(httpServletRequest.getParameter("id"));
         String password = httpServletRequest.getParameter("password");

         try(Session session = HibernateConfig.getSessionFactory().openSession())
         {
             Query query = session.createQuery("from Employee where id=:id and password=:password");
             query.setParameter("id",id);
             query.setParameter("password",password);
             employee=(Employee)query.uniqueResult();
             String post=employee.getPost();

             if(post.equalsIgnoreCase("admin"))
             {
                 requestDispatcher=httpServletRequest.getRequestDispatcher("admin");
                 httpServletRequest.setAttribute("name",employee.getName());
                 requestDispatcher.forward(httpServletRequest,httpServletResponse);

             }
             else if(post.equalsIgnoreCase("employee"))
             {
                 requestDispatcher=httpServletRequest.getRequestDispatcher("employee");
                 httpServletRequest.setAttribute("name",employee.getName());
                 requestDispatcher.forward(httpServletRequest,httpServletResponse);

             }
             else if(post.equalsIgnoreCase("manager"))
             {
                 requestDispatcher=httpServletRequest.getRequestDispatcher("manager");
                 httpServletRequest.setAttribute("name",employee.getName());
                 requestDispatcher.forward(httpServletRequest,httpServletResponse);
             }
             else
                 {
                     httpServletResponse.getWriter().println("Invalid Entries");
                     requestDispatcher=httpServletRequest.getRequestDispatcher("Login.html");
                     requestDispatcher.include(httpServletRequest,httpServletResponse);
                 }
         }
         catch (Exception e)
         {
             logger.error(e);

         }
     }
     @Override
     public void destroy()
     {
       HibernateConfig.shutdown();
     }
 }


