<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error Page</title>
</head>
<body>
<div style="height:30%;width: 100%;background-color: coral;
            font-size: 30px;margin: 0;">
<h2 align="center"><spring:message code="label.error"/></h2>
</div>
<div style="height:70%;width:100%;background-color: powderblue;
            font-size: 22px;margin:0px;">
    <h4 align="center"><spring:message code="label.checkDetail"/></h4>
    <a href="/"><h5 align="center" style="color: darkblue"><spring:message code="label.login"/></h5></a>
</div>

</body>
</html>
