<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UpdateSuccess</title>
</head>
<body>
<div style="height: 100%;width: 100%; margin: 0px;font-size: 25px;
               color: firebrick;background-color: burlywood;">
    <h4 align="center">${success.firstName} ${success.lastName}</h4>
    <h3 align="center">Your Profile Updated Successfully</h3>
    <a href="/"><h5 align="center" style="color: darkblue">Login</h5></a>
</div>

</body>
</html>
