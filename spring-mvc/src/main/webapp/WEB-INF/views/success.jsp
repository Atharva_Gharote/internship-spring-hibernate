<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Success</title>
</head>
<body>
 <div style="height:100%;width:100%;font-size: 20px;margin: 0px;
             background-color:burlywood">
   <h3 align="center">Registration Successful....</h3>
    <h4 style="color: firebrick" align="center">Remember the id it helps you login </h4>
    <table align="center" border="0px">
        <tr>
            <td>Id:</td>
            <td>${employeeSuccess.id}</td>
        </tr>
        <tr>
            <td> FirstName:</td>
            <td>${employeeSuccess.firstName}</td>
        </tr>
        <tr>
            <td> LastName:</td>
            <td>${employeeSuccess.lastName}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>${employeeSuccess.email}</td>
        </tr>
        <tr>
            <td>Post:</td>
            <td>${employeeSuccess.post}</td>
        </tr>
        <tr>
            <td>MobileNumber:</td>
            <td>${employeeSuccess.mobileNumber}</td>
        </tr>
        <tr>
            <td>Password:</td>
            <td>${employeeSuccess.password}</td>
        </tr>
    </table>
     <a href="/"><h5 align="center" style="color: darkblue">Login</h5></a>
 </div>
</body>
</html>
