<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration Page</title>
    <style type="text/css">
        .error
        {
            color: red;
        }
        body
        {
            background: #736d72;
        }
        .title
        {
            color:black;
            line-height: 30px;
            font-size: 25px;
            font-weight: 300;
            text-align: center;
        }
        .register
        {
            margin: 20px auto;
            width: 300px;
            padding: 30px 25px;
            background: white;
            border: 1px solid #c4c4c4;
        }
        h3.register-title
        {
            margin: -28px -25px 25px;
            padding: 15px 25px;
            line-height: 30px;
            font-size: 25px;
            font-weight: 300;
            color: #ADADAD;
            text-align:center;
            background: #f7f7f7;

        }
        .register-input
        {
            width: 285px;
            height: 50px;
            margin-bottom: 25px;
            padding-left:10px;
            font-size: 15px;
            background: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .register-input:focus
        {
            border-color:#6e8095;
            outline: none;
        }
        .register-button
        {
            width: 100%;
            height: 50px;
            padding: 0;
            font-size: 20px;
            color: #fff;
            text-align: center;
            background: #f0776c;
            border: 0;
            border-radius: 5px;
            cursor: pointer;
            outline:0;
        }
    </style>
</head>
<body>
    <h1 class="title">Welcome to LocationGuru Solutions</h1>
    <h3 align="center"><a href="registerPage?lang=en">English</a> | <a href="registerPage?lang=fr">French</a></h3>
    <form:form class="register"  action="registerProcess" method="get" modelAttribute="registerEmployee">
        <caption align="top"><h3 align="center"><spring:message code="label.register.header"/></h3>
        </caption>


        <spring:message code="label.firstName"/>
        <form:input class="register-input" path="firstName"/>
        <form:errors path="firstName" cssClass="error"/>

        <spring:message code="label.lastName"/>
        <form:input class="register-input" path="lastName"/>
        <form:errors path="lastName" cssClass="error"/>

        <spring:message code="label.email"/>
        <form:input class="register-input" path="email" />
        <form:errors  path="email" cssClass="error"/>

       <spring:message code="label.post"/>
        <form:select class="register-input" path="post">
            <form:option value="Select"/>
            <form:option value="Admin" />
            <form:option value="Manager" />
            <form:option value="Employee" />
        </form:select>

       <spring:message code="label.mobileNumber"/>
        <form:input class="register-input" path="mobileNumber" />
        <form:errors  path="mobileNumber" cssClass="error"/>

        <spring:message code="label.password"/>
        <form:password class="register-input" path="password"/>
        <form:errors path="password" cssClass="error"/>

        <input class="register-button" type="submit" value="<spring:message code="label.register"/>">
    </form:form>
</body>
</html>