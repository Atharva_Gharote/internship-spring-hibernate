<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<div style="height:100%;width:100%;font-size: 20px;margin: 0;
            background-color:burlywood">
    <h3 align="center"> ${employeeProfile.post} Profile</h3>
    <h4 style="color: firebrick" align="center">Remember the id it helps you login </h4>
    <table align="center" border="0px">
        <tr>
            <td>Id:</td>
            <td>${employeeProfile.id}</td>
        </tr>
        <tr>
            <td> FirstName:</td>
            <td>${employeeProfile.firstName}</td>
        </tr>
        <tr>
            <td> LastName:</td>
            <td>${employeeProfile.lastName}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>${employeeProfile.email}</td>
        </tr>
        <tr>
            <td>Post:</td>
            <td>${employeeProfile.post}</td>
        </tr>
        <tr>
            <td>MobileNumber:</td>
            <td>${employeeProfile.mobileNumber}</td>
        </tr>
        <tr>
            <td>Password:  </td>
            <td>${employeeProfile.password}</td>
        </tr>
    </table>
    <a href="/"><h4 align="center">Logout</h4></a>
</div>
</body>
</html>
