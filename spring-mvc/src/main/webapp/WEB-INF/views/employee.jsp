<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee Page</title>
</head>
<body>
<div style="height:100px;width:100%;font-size:40px;margin: 0;
              background-color:cadetblue">
    <h3 align="center" style="font-size: 30px;">Employee</h3>
</div>

<div style="height:700px;width: 100%;margin: 0;
              font-size:25px;
              color:firebrick">
    <table align="center">
        <tr><h3 align="center">Login Successful.......</h3></tr>

        <tr><h4 align="center">Welcome ${employee1.firstName} ${employee1.lastName}</h4></tr>

        <a href="profile/profileProcess"><h4 align="center">View Profile</h4></a>
        <a href="profile/updatePage"><h4 align="center">Update Profile</h4></a>

    </table>
    <a href="/"><h5 align="center" style="color: darkblue">Logout</h5></a>
</div>
</body>
</html>
