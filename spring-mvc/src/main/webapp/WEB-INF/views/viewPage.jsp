<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Employee Details</title>
</head>
 <body>
   <div style="height:100%;width:100%;font-size: 20px;margin: 0px;
           background-color: #f7f7f7;">
    <h2 style="font-size: 20px;text-align: center">Employee Details</h2>
     <table align="center" border="5px" style="color:#000;">
        <tr>
            <th>Id</th>
            <th>FirstName</th>
            <th>lastName</th>
            <th>Email</th>
            <th>Post</th>
            <th>MobileNumber</th>
            <th>Password</th>
        </tr>
        <c:forEach var="viewEmployee" items="${list}">
            <tr>
                <td>${viewEmployee.id}</td>
                <td>${viewEmployee.firstName}</td>
                <td>${viewEmployee.lastName}</td>
                <td>${viewEmployee.email}</td>
                <td>${viewEmployee.post}</td>
                <td>${viewEmployee.mobileNumber}</td>
                <td>${viewEmployee.password}</td>
            </tr>
        </c:forEach>
     </table>
       <h5 align="center"><label>Note:</label>Please Enter page number in link to get more result</h5>
       <a href="/"><h5 align="center" style="color: darkblue">Logout</h5></a>
   </div>
 </body>
</html>
