<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Page</title>
    <style type="text/css">
        .error
        {
            color: red;
        }
        body
        {
            background: #667775;
        }
        .title
        {
            color:black;
            line-height: 30px;
            font-size: 25px;
            font-weight: 300;
            text-align: center;
        }
        .login
        {
            margin: 20px auto;
            width: 300px;
            padding: 30px 25px;
            background: white;
            border: 1px solid #c4c4c4;
        }
        h3.login-title
        {
            margin: -28px -25px 25px;
            padding: 15px 25px;
            line-height: 30px;
            font-size: 25px;
            font-weight: 300;
            color: #ADADAD;
            text-align:center;
            background: #f7f7f7;

        }
        .login-input
        {
            width: 285px;
            height: 50px;
            margin-bottom: 25px;
            padding-left:10px;
            font-size: 15px;
            background: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .login-input:focus
        {
            border-color:#6e8095;
            outline: none;
        }
        .login-button
        {
            width: 100%;
            height: 50px;
            padding: 0;
            font-size: 20px;
            color: #fff;
            text-align: center;
            background: #f0776c;
            border: 0;
            border-radius: 5px;
            cursor: pointer;
            outline:0;
        }
        .register
        {
            text-align:center;
            margin-bottom:0px;
        }
        .register a
        {
            color:#666;
            text-decoration:darksalmon;
            font-size:18px;
        }
    </style>
</head>
<body>
<h1 class="title">Welcome to LocationGuru Solutions</h1>
<h3 align="center"><a href="/?lang=en">English</a> | <a href="/?lang=fr">French</a></h3>
<form:form class="login"  action="loginProcess" method="post" modelAttribute="employee">
        <caption align="top"><h3 align="center"><spring:message code="label.login.header"/></h3>
        </caption><br>

          <spring:message code="label.id"/>
          <form:input class="login-input" path="id" />
          <form:errors path="id" cssClass="error"/>

          <spring:message code="label.password"/>
          <form:password class="login-input" path="password"/>
          <form:errors path="password" cssClass="error"/>

        <input class="login-button" type="submit" value="<spring:message code="label.login"/>">
        <p class="register"><a href="registerPage"><spring:message code="label.registerHere"/></a></p>
</form:form>
</body>
</html>