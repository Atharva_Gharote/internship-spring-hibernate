<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Login Failed</title>
</head>
<body>

<div style="height:60px; width:100%;font-size: 30px; margin:0;">
    <h2 align="center"><spring:message code="label.loginFail"/>
    </h2>
</div>
<div style="height:100px; width:100%;font-size: 25px;margin:0;">
    <h3 align="center"><spring:message code="label.invalidCredential"/></h3>
    <a href="/"><h5 align="center" style="color: darkblue">Login</h5></a>
</div>
</body>
</html>
