<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Admin Page</title>
</head>
<body>
  <div style="height:100px;width:100%;margin:0;
               font-size:40px;
              background-color:cadetblue">
    <h2 align="center" style="font-size: 40px;">Admin</h2>
  </div>
  <div style="height:700px;
              width: 100%;
              font-size:25px;
              margin: 0;
              color: firebrick">
    <table align="center">
      <tr>
        <h3 align="center">Login Successful.......</h3></tr>

      <tr><h4 align="center">Welcome ${employeeAdmin.firstName} ${employeeAdmin.lastName}</h4></tr>
      <a href="profile/profileProcess"><h4 align="center">View Profile</h4></a>
      <a href="profile/viewProcess?pageNo="><h4 align="center">Get Employees Details</h4></a>

      <tr align="center">
          <form action="profile/searchBy">
              <label>Enter firstName of Employee</label>
              <input type="text" name="firstName">
              <input type="submit" value="search">
          </form>
      </tr>

      <a href="profile/updatePage"><h4 align="center">Update</h4></a>
    </table>
    <a href="/"><h5 align="center" style="color: darkblue">Logout</h5></a>
  </div>
</body>
</html>
