package com.locationguru.mvccontroller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController
 {
     @ExceptionHandler(value = RuntimeException.class)
     public String exceptionHandler()
     {
         return "error";
     }
 }
