package com.locationguru.mvccontroller;
import com.locationguru.model.Employee;
import com.locationguru.serviceimplementations.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/profile")
public class EmployeeProfileController
 {
     private static final Logger logger = LogManager.getLogger(EmployeeProfileController.class);
     @Autowired
     private EmployeeService employeeService;

     public EmployeeService getEmployeeService()
     {
         return employeeService;
     }

     @GetMapping(value = "/profileProcess")
     public ModelAndView viewProfile(@SessionAttribute(value = "employee") Employee employee)
     {
         logger.info("Profile Details");
         Integer id = employee.getId();
         String password = employee.getPassword();
         Employee employee1 = getEmployeeService().findEmployeeByIdAndPassword(id,password);
         ModelAndView modelAndView = new ModelAndView("profile");
         modelAndView.addObject("employeeProfile",employee1);
         return modelAndView;
     }

     @GetMapping(value = "/updatePage")
     public  String updatePage(@SessionAttribute(value = "employee")Employee employee, Model model)
     {
         model.addAttribute("employee",employee);
         return "update";
     }

     @PostMapping(value = "/updateProcess")
     public ModelAndView updateProcess(@Valid @ModelAttribute(value = "employee")Employee employee1, BindingResult bindingResult)
     {
         if(bindingResult.hasErrors())
         {
             return new ModelAndView("update");
         }
         else
         {
             getEmployeeService().updateEmployee(employee1);
             ModelAndView modelAndView = new ModelAndView("updateSuccess");
             modelAndView.addObject("success", employee1);
             return modelAndView;
         }
     }

     @GetMapping(value = "/viewProcess")
     public ModelAndView viewAllEmployee(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNumber)
     {
       ModelAndView modelAndView = new ModelAndView("viewPage");
       List list = getEmployeeService().getAllEmployees(pageNumber);
       modelAndView.addObject("list",list);
       return modelAndView;
     }

     @GetMapping(value = "/searchBy")
     public ModelAndView viewEmployeesByFirstName(@RequestParam(name = "firstName") String firstName)
     {
         ModelAndView modelAndView = new ModelAndView("viewPage");
         List list = getEmployeeService().searchByName(firstName);
         modelAndView.addObject("list",list);
         return modelAndView;
     }
 }

