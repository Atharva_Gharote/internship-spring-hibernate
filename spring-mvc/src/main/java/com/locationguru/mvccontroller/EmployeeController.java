package com.locationguru.mvccontroller;
import com.locationguru.model.Employee;
import com.locationguru.serviceimplementations.EmployeeService;
import com.locationguru.serviceimplementations.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

@Controller
@SessionAttributes(value = "employee")
public class EmployeeController
 {
     @Autowired
     private EmployeeService employeeService;
     @Autowired
     private MailService mailService;

     public EmployeeService getEmployeeService()
     {
         return employeeService;
     }

     public MailService getMailService()
     {
         return mailService;
     }

     @GetMapping(value = "/")
     public  String loginPage(Model model)
     {
         model.addAttribute("employee",new Employee());
         return "login";
     }
     @PostMapping(value = "/loginProcess")
     public ModelAndView loginProcess(@Valid @ModelAttribute(value = "employee") Employee employee,BindingResult bindingResult)
     {
         if(bindingResult.hasErrors())
         {
            return new ModelAndView("login");
         }
         else
         {
             Integer id = employee.getId();
             String password = employee.getPassword();
             ModelAndView modelAndView;

             Employee employee1 = getEmployeeService().findEmployeeByIdAndPassword(id,password);
             String post = employee1.getPost();

             if ((employee1.getId().equals(id)) && (employee1.getPassword().equals(password)))
             {
                 if (post.equalsIgnoreCase("admin"))
                 {
                     modelAndView = new ModelAndView("admin");
                     modelAndView.addObject("employeeAdmin", employee1);
                 } else if ((post.equalsIgnoreCase("manager")))
                 {
                     modelAndView = new ModelAndView("manager");
                     modelAndView.addObject("employeeManager", employee1);
                 }
                 else
                 {
                     modelAndView = new ModelAndView("employee");
                     modelAndView.addObject("employee1", employee1);
                 }
             }
             else
             {
                 modelAndView = new ModelAndView("fail");
             }
             return modelAndView;
         }
     }

     @GetMapping(value = "/registerPage")
     public  String registerPage(Model model)
     {
         model.addAttribute("registerEmployee",new Employee());
         return "register";
     }

     @GetMapping(value ="/registerProcess")
     public ModelAndView registerSuccess(@Valid @ModelAttribute(value = "registerEmployee") Employee employee, BindingResult bindingResult)
     {
         if(bindingResult.hasErrors())
         {
             return new ModelAndView("register");
         }
         else
         {
             getEmployeeService().createEmployee(employee);
             String userEmail = employee.getEmail();
             String emailBody="Your Id is"+employee.getId()+"\n Remember Id for next time login";
             getMailService().sendMail(userEmail,emailBody);
             ModelAndView modelAndView = new ModelAndView("success");
             modelAndView.addObject("employeeSuccess", employee);
             return modelAndView;
         }
     }
 }
