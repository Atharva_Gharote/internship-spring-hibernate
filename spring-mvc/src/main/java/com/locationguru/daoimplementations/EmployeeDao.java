package com.locationguru.daoimplementations;
import com.locationguru.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface EmployeeDao extends JpaRepository<Employee,Integer>
{
     @Query("select employee from Employee employee where employee.id=?1 and employee.password=?2")
     Employee findByIdAndPassword(Integer id,String password);
     @Query("select employee from Employee employee order by employee.id asc ")
     Page<Employee> findAllBy(Pageable pageable);
     List<Employee> findAllByFirstName(String firstName);
}

