package com.locationguru.serviceimplementations;
import com.locationguru.daoimplementations.EmployeeDao;
import com.locationguru.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service("employeeService")
public  class EmployeeService
 {
     @Autowired
     private EmployeeDao employeeDao;

     public void setEmployeeDao(EmployeeDao employeeDao)
     {
         this.employeeDao = employeeDao;
     }

     public EmployeeDao getEmployeeDao()
     {
         return employeeDao;
     }

     @Transactional
     public void createEmployee(Employee employee)
     {
         getEmployeeDao().save(employee);
     }

     @Transactional
     public void updateEmployee(Employee employee)
     {
         getEmployeeDao().save(employee);
     }

     public Employee findEmployeeByIdAndPassword(Integer id,String password)
     {
         return getEmployeeDao().findByIdAndPassword(id,password);
     }

     public List<Employee> getAllEmployees(Integer pageNumber)
     {
         Integer elementPerPage = 10;
         Pageable pageable = PageRequest.of(pageNumber-1,elementPerPage);
         return employeeDao.findAllBy(pageable).getContent();
     }

     public List<Employee> searchByName(String firstName)
     {
         return employeeDao.findAllByFirstName(firstName);
     }
 }
