package com.locationguru.serviceimplementations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("mailService")
public class MailService
 {
     @Autowired
     private JavaMailSender mailSender;

     public void sendMail(String to,String body) throws MailException
     {
         SimpleMailMessage message = new SimpleMailMessage();
         message.setFrom("atharvagharote@gmail.com");
         message.setTo(to);
         message.setSubject("Registration Confirmation");
         message.setText(body);
         mailSender.send(message);
     }
 }
