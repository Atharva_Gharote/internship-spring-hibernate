package com.locationguru.model;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name ="employee_details",schema = "internship_samples")
public class Employee
 {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     @Column(name = "id")
     @NotNull
     private Integer id;
     @Column(name = "first_name")
     @NotEmpty
     private String firstName;
     @Column(name = "last_name")
     @NotEmpty
     private String lastName;
     @Column(name = "email")
     @NotEmpty
     @Email
     private String email;
     @Column(name = "post")
     @NotEmpty
     private String post;
     @Column(name= "mobile_number")
     @NotNull
     private Long mobileNumber;
     @Column(name = "password")
     @NotEmpty
     private String password;

     public Employee()
     {

     }

     public Integer getId()
     {
         return id;
     }

     public void setId(Integer id)
     {
         this.id = id;
     }

     public String getFirstName()
     {
         return firstName;
     }

     public void setFirstName(String firstName)
     {
         this.firstName = firstName;
     }

     public String getLastName()
     {
         return lastName;
     }

     public void setLastName(String lastName)
     {
         this.lastName = lastName;
     }

     public String getEmail()
     {
         return email;
     }

     public void setEmail(String email)
     {
         this.email = email;
     }

     public void setPost(String post)
     {
         this.post = post;
     }

     public String getPost()
     {
         return post;
     }

     public Long getMobileNumber()
     {
         return mobileNumber;
     }

     public void setMobileNumber(Long mobileNumber)
     {
         this.mobileNumber = mobileNumber;
     }

     public void setPassword(String password)
     {
         this.password = password;
     }

     public String getPassword()
     {
         return password;
     }
 }
