package com.locationguru.configurations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import java.util.Locale;
import java.util.Properties;

@EnableWebMvc
@ComponentScan(basePackages = {"com.locationguru"})
@Configuration
public class WebConfiguration implements WebMvcConfigurer
 {
    @Bean
    public InternalResourceViewResolver internalResourceViewResolver()
    {
        InternalResourceViewResolver viewResolver=new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    @Bean
    public ResourceBundleMessageSource messageSource()
    {
        ResourceBundleMessageSource resourceBundleMessageSource =new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasename("config/errorMessages");
        resourceBundleMessageSource.setBasename("config/messages");
        return resourceBundleMessageSource;
    }

     @Bean("interceptor")
     public Interceptor getInterceptor()
     {
         return new Interceptor();
     }

     @Override
     public void addInterceptors(InterceptorRegistry registry)
     {
         LocaleChangeInterceptor localeChangeInterceptor=new LocaleChangeInterceptor();
         localeChangeInterceptor.setParamName("lang");
         registry.addInterceptor(localeChangeInterceptor);
         registry.addInterceptor(getInterceptor());
     }

     @Bean
     public CookieLocaleResolver localeResolver()
     {
         CookieLocaleResolver cookieLocaleResolver=new CookieLocaleResolver();
         cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
         cookieLocaleResolver.setCookieName("myLocalCookie");
         cookieLocaleResolver.setCookieMaxAge(3600);
         return cookieLocaleResolver;
     }

     @Bean("mailSender")
     public JavaMailSenderImpl mailSender()
     {
         JavaMailSenderImpl javaMailSender=new JavaMailSenderImpl();
         javaMailSender.setHost("smtp.gmail.com");
         javaMailSender.setPort(465);
         javaMailSender.setUsername("atharvagharote@gmail.com");
         javaMailSender.setPassword("");
         javaMailSender.setJavaMailProperties(properties());
         return javaMailSender;
     }
     @Bean
     public Properties properties()
     {
         Properties properties=new Properties();
         properties.setProperty("mail.transport.protocol","smtp");
         properties.setProperty("mail.smtp.auth","true");
         properties.setProperty("mail.smtp.starttls.enable","true");
         properties.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
         return properties;
     }
 }
