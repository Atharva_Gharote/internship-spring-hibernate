package com.locationguru.configurations;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Interceptor extends HandlerInterceptorAdapter
 {
     private static final Logger logger = LogManager.getLogger(Interceptor.class);

     @Override
     public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
             throws Exception
     {
         logger.debug("preHandle operation started");
         return true;
     }
     @Override
     public void postHandle(HttpServletRequest request, HttpServletResponse response,
                                Object handler, ModelAndView modelAndView) throws Exception
     {
         logger.debug("postHandle operation started");
     }
     @Override
     public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                 Object handler, Exception ex) throws Exception
     {
         logger.debug(" operation Completed");
     }

 }
