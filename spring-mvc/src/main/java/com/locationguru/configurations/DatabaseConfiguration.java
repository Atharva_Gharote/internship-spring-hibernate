package com.locationguru.configurations;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@EnableTransactionManagement
@EnableWebMvc
@ComponentScan(basePackages = {"com.locationguru"})
@Configuration
@EnableJpaRepositories("com.locationguru.daoimplementations")
public class DatabaseConfiguration
 {

     @Bean("dataSource")
     public DriverManagerDataSource dataSource()
     {
         DriverManagerDataSource driverManagerDataSource=new DriverManagerDataSource();
         driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
         driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
         driverManagerDataSource.setUsername("postgres");
         driverManagerDataSource.setPassword("2317");
         return driverManagerDataSource;
     }

     @Bean("entityManagerFactory")
     @Autowired
     public LocalContainerEntityManagerFactoryBean  entityManagerFactory(DriverManagerDataSource dataSource)
     {
         LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
         entityManagerFactoryBean.setDataSource(dataSource);
         entityManagerFactoryBean.setPackagesToScan("com.locationguru.model");
         entityManagerFactoryBean.setJpaProperties(hibernateProperties());
         entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
         return entityManagerFactoryBean;
     }

     @Bean("hibernateProperties")
     public Properties hibernateProperties()
     {
         Properties properties =new Properties();
         properties.setProperty("connection.provider_class","org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
         properties.setProperty("hibernate.dialect","org.hibernate.dialect.PostgreSQL94Dialect");
         properties.setProperty("hibernate.show_sql","true");
         properties.setProperty("hibernate.hbm2ddl.auto","update");
         properties.setProperty("hibernate.hikari.connectionTimeout","20000");
         properties.setProperty("hibernate.hikari.minimumIdle","10");
         properties.setProperty("hibernate.hikari.maximumPoolSize","40");
         properties.setProperty("hibernate.hikari.idleTimeout","200000");
         properties.setProperty("cache.use_second_level_cache","true");
         properties.setProperty("cache.region.factory_class","org.hibernate.cache.ehcache.EhCacheRegionFactory");
         return properties;
     }

     @Bean("transactionManager")
     @Autowired
     public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory)
     {
         JpaTransactionManager transactionManager = new JpaTransactionManager();
         transactionManager.setEntityManagerFactory(entityManagerFactory);
         return transactionManager;
     }
 }
