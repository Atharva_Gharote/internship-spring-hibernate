package mvccontroller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class WelcomeController implements Controller
 {
      @Override
      public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception
      {
        String name=httpServletRequest.getParameter("name");
        Map map = new HashMap();
        map.put("message","Welcome  " +name+ "   Have a good day");

        ModelAndView modelAndView= new ModelAndView("success",map);
        return modelAndView;
      }
 }
