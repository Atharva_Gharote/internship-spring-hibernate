package locationguru.iocimplementations.iocbeantype;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class BeanTypeTest
 {
     public static void main(String args[])
     {
         Resource resource = new ClassPathResource("spring.xml");
         BeanFactory factory = new XmlBeanFactory(resource);
         HelloMessage helloMessage =(HelloMessage)factory.getBean("names");
         helloMessage.display();
     }
 }
