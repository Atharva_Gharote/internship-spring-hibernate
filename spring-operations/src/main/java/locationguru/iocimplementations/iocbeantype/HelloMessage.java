package locationguru.iocimplementations.iocbeantype;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HelloMessage
{
    private static final Logger logger = LogManager.getLogger(HelloMessage.class);
    private String name;
    public HelloMessage()
    {
        logger.debug("Object created");
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void display()
    {
        logger.info("HelloMessage {}",name);
    }
}
