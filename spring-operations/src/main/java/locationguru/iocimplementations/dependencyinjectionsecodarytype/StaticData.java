package locationguru.iocimplementations.dependencyinjectionsecodarytype;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StaticData
 {
     private static String companyCity;
     private static final Logger logger = LogManager.getLogger(StaticData.class);


     public static void setCompanyCity(String companyCity)
     {
         StaticData.companyCity = companyCity;
     }

     public static void display()
     {
         logger.info("CompanyCity :{}",companyCity);
     }
 }
