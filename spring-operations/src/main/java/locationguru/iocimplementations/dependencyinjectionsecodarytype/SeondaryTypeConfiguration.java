package locationguru.iocimplementations.dependencyinjectionsecodarytype;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SeondaryTypeConfiguration

 {
     public static void main(String args[])
     {
          ApplicationContext applicationContext = new ClassPathXmlApplicationContext("employeeconfig.xml");
          EmployeeDetails employeeDetails =(EmployeeDetails)applicationContext.getBean("emp");
          employeeDetails.printData();

          ApplicationContext applicationContext1 = new ClassPathXmlApplicationContext("staticdata.xml");
          applicationContext1.getBean("data");
          StaticData.display();


     }
 }
