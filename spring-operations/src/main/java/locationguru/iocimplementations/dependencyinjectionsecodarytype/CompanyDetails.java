package locationguru.iocimplementations.dependencyinjectionsecodarytype;

public class CompanyDetails
 {
      private   String companyName;

      public void setCompanyName(String companyName)
      {
          this.companyName=companyName;
      }
      public String getCompanyName()
      {
          return companyName;
      }
 }
