package locationguru.iocimplementations.dependencyinjectionsecodarytype;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.*;

public class EmployeeDetails
 {
     private String employeeName[];
     private CompanyDetails companyDetails[];
     private static final Logger logger = LogManager.getLogger(EmployeeDetails.class);

     public void setEmployeeName(String[] employeeName)
     {

         this.employeeName=employeeName;
     }
     public void setCompanyDetails(CompanyDetails[] companyDetails)
     {
         this.companyDetails = companyDetails;
     }

     public void printData()
     {
         for (String employeeName1:employeeName)
         {
             logger.info(employeeName1);
         }
         for(CompanyDetails company: companyDetails)
         {
             logger.info(company.getCompanyName());
         }
     }

 }

