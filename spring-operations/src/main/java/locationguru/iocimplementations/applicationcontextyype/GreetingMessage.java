package locationguru.iocimplementations.applicationcontextyype;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GreetingMessage
 {
     private String greet;
     private String name;
     private static final Logger logger = LogManager.getLogger(GreetingMessage.class);

     public GreetingMessage(String greet, String name)
     {
         this.greet=greet;
         this.name=name;
     }

     public void displayGreet()
     {
         logger.info("Good  {} {}",greet,name);
     }
 }
