package locationguru.iocimplementations.applicationcontextyype;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextTest
 {
     private static final Logger logger = LogManager.getLogger(ApplicationContextTest.class);

     public static void main(String args[])
     {
         ConfigurableApplicationContext applicationContext =new ClassPathXmlApplicationContext("Spring.xml");
         GreetingMessage greetingMessage =(GreetingMessage) applicationContext.getBean("greetings");
         GreetingMessage greetingMessage1 =(GreetingMessage)applicationContext.getBean("greetings");
         greetingMessage1.displayGreet();
         greetingMessage.displayGreet();

        Operator operator=(Operator)applicationContext.getBean("op");
        logger.info("Sim Name {}",operator.mySim().getName());
        applicationContext.close();
     }
 }
