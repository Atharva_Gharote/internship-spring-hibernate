package locationguru.iocimplementations.applicationcontextyype;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Sim
 {
    private String name;
    private static final Logger logger = LogManager.getLogger(Sim.class);

    @PostConstruct
     public void init()
    {
     logger.debug("Look up method introduced ");
    }

     public void setName(String name)
     {
     this.name = name;
     }

    public String getName()
    {
     return name;
    }

    @PreDestroy
    public void destroy()
    {
     logger.debug("Look up completed");
    }
 }
