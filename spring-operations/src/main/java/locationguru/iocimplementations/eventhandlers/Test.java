package locationguru.iocimplementations.eventhandlers;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test
 {
     public static void main(String args[])
     {
         ConfigurableApplicationContext configurableApplicationContext =new ClassPathXmlApplicationContext("listner.xml");
         configurableApplicationContext.start();
         configurableApplicationContext.stop();
         configurableApplicationContext.close();
     }

 }
