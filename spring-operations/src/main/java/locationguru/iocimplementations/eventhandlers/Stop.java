package locationguru.iocimplementations.eventhandlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;

public class Stop implements ApplicationListener<ContextStoppedEvent>
 {
     private static final Logger logger = LogManager.getLogger(Stop.class);

     @Override
     public void onApplicationEvent(ContextStoppedEvent event)
     {
         logger.debug("Application Context Stopped");

     }
 }
