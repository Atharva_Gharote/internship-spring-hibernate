package locationguru.iocimplementations.eventhandlers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class Close implements ApplicationListener<ContextClosedEvent>
{
    private static final Logger logger = LogManager.getLogger(Close.class);

    @Override
    public void onApplicationEvent(ContextClosedEvent contextStartedEvent)
    {
      logger.debug("Application Context Closed");

    }
}

