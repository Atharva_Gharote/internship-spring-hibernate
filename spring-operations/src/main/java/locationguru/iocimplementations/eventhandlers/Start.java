package locationguru.iocimplementations.eventhandlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;

public class Start implements ApplicationListener<ContextStartedEvent>
{
    private static final Logger logger = LogManager.getLogger(Start.class);

    @Override
    public void onApplicationEvent(ContextStartedEvent contextStartedEvent)
    {
        logger.debug("Application Context Started");

    }
}