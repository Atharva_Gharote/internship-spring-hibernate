package locationguru.iocimplementations.autowire;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

@Component
public class StudentDetails
 {
     @Resource
     @Qualifier("student")
     private Student student;
     private static final Logger logger = LogManager.getLogger(StudentDetails.class);

     @PostConstruct
     public void init()
     {
         logger.debug("Student Details are loading ");
     }


     public void display()
     {
         logger.info("Student Name {} Field {}",student.getName(), student.getField());
     }

     @PreDestroy
     public void destroy()
     {
         logger.debug("Details are inserted Sucessfully");
     }
 }

