package locationguru.iocimplementations.autowire;

public class Student
 {
     private String name;
     private String field;


     public void setName(String name)
     {
         this.name = name;
     }

     public String getName()
     {
         return name;
     }

     public void setField(String field)
     {
         this.field = field;
     }

     public String getField()
     {
         return field;
     }
 }
