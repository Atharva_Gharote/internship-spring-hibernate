package locationguru.iocimplementations.autowire;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutoWireConfig

 {
     public static void main(String args[])
     {
         ConfigurableApplicationContext applicationContext1 = new ClassPathXmlApplicationContext("autowire.xml");
         StudentDetails studentDetails = applicationContext1.getBean(StudentDetails.class);
         studentDetails.display();
         applicationContext1.close();

     }
 }
