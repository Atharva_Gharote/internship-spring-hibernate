package locationguru.iocimplementations.namespace;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class NameSpacesConfiguration
 {
     public static void main(String args[])
     {
         ApplicationContext applicationContext1=new ClassPathXmlApplicationContext("namespaces.xml");
         ApplicationContext applicationContext=new ClassPathXmlApplicationContext("namespaces.xml");
         CarDetails carDetails =(CarDetails)applicationContext1.getBean("carDetails");
         CarDetails carDetails1=(CarDetails)applicationContext1.getBean("carDetails");
         CarName carName=(CarName)applicationContext.getBean("name");
         carDetails.displayCar();
         carDetails1.displayCar();
         carName.displayCar();

     }
 }
