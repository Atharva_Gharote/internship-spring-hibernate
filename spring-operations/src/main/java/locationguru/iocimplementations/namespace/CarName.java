package locationguru.iocimplementations.namespace;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CarName
 {
     private String carName;
     private static final Logger logger = LogManager.getLogger(CarDetails.class);

     public CarName()
     {
         logger.debug("CarName object Created");
     }
     public void setCarName(String carName)
     {
         this.carName=carName;
     }
     public void displayCar()
     {
         logger.info(" CarName-{}",carName);
     }
 }
