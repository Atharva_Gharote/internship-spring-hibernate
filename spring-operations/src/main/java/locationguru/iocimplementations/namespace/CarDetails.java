package locationguru.iocimplementations.namespace;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CarDetails
 {

     private String  carNumber;
     private int modelNumber;
     private static final Logger logger = LogManager.getLogger(CarDetails.class);

     public CarDetails()
     {
         logger.debug("CarDetails Object Created");
     }

     public CarDetails(String carNumber, int modelNumber)
     {
         this.carNumber=carNumber;
         this.modelNumber=modelNumber;

     }

     public void displayCar()
     {
         logger.info(" CarNo-{} ModelNo-{}",carNumber,modelNumber);
     }
 }
