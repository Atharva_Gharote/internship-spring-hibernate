package locationguru.iocimplementations.factorymethods;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test
 {
     public static void main(String args[])
     {
         ConfigurableApplicationContext applicationContext=new ClassPathXmlApplicationContext("factory.xml");
         Network network=(Network)applicationContext.getBean("factory");
         network.speed();
         applicationContext.close();
     }
 }
