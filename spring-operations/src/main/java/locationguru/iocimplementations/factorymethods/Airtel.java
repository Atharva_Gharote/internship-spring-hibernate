package locationguru.iocimplementations.factorymethods;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

 public class Airtel implements Network
 {
     private static final Logger logger = LogManager.getLogger(Airtel.class);

     @PostConstruct
     public void init()
     {
         logger.debug("Starting....");
     }
     public void speed()
     {
      logger.info("4G is available");
     }

     @PreDestroy
     public void destroy()
     {
         logger.debug("Closing....");
     }
 }
