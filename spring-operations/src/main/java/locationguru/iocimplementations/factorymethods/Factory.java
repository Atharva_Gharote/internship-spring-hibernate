package locationguru.iocimplementations.factorymethods;

public class Factory
 {
     private static String simName;

     public static void setSimName(String simName)
     {
       Factory.simName=simName;
     }

     public  static Network getInstance()throws Exception
     {
       Network network=(Network)Class.forName(simName).newInstance();
       return network;
     }
 }
